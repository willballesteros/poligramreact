import React from 'react';
import ReactDOM from 'react-dom';
import firebase from 'firebase';
import './index.css';
import App from './App';
//import registerServiceWorker from './registerServiceWorker';

firebase.initializeApp({
    apiKey: "AIzaSyBVzp08Bhhnw661nj9KfDWwtUQH1TD94H0",
    authDomain: "pseudogram-49a7d.firebaseapp.com",
    databaseURL: "https://pseudogram-49a7d.firebaseio.com",
    projectId: "pseudogram-49a7d",
    storageBucket: "pseudogram-49a7d.appspot.com",
    messagingSenderId: "431819718701"
});

ReactDOM.render(
    <App />, 
    document.getElementById('root')
);
//registerServiceWorker();
